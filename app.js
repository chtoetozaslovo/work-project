let express = require('express');
let path = require('path');
let logger = require('morgan');
let bodyParser = require('body-parser');
let neo4j = require('neo4j-driver');

let app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

let driver = neo4j.driver('bolt://localhost', neo4j.auth.basic('neo4j', '123'));
let session = driver.session();

app.get('/', function(req, res)
  {
    session
      .run('match(n:Error) return n limit 25')
      .then(function(result)
        {
          let errArr = [];
          let messageArr = [];

          result.records.forEach(function(record)
            {
              errArr.push(
                {
                  id: record._fields[0].low,
                  name: record._fields[0].properties.name
                });
                
              //console.log(record._fields[0].properties);
            });

            session
            .run('match (n:Message) return n')
            .then(function(result2)
              {
                result2.records.forEach(function(record)
                {
                messageArr.push(
                  {
                    id: record._fields[0].low,
                    lang: record._fields[0].properties.name,
                    lable: record._fields[0].properties.lable,
                    errId: record._fields[0].properties.errId
                  });
                  //console.log(record._fields[0].properties.errId);
                });
                res.render('index', {
                  errors: errArr,
                  messages: messageArr
                });
                //session.close();
              })
              
            .catch(function(err){
              console.log(err)
          });
        })
      .catch(function(err)
        {
          console.log(err);
        });
  });

app.post('/err/add', function(req, res)
  {
    let errCode = req.body.errName;

    //console.log(errCode);

    session
      .run('create (n:Error {name: $errParam})', {errParam: errCode})
      .then(function(result)
        {
          res.redirect('/');
          //session.close();
        })
      .catch(function(err){
        console.log(err)
      });
    
    res.redirect('/');
  });

  app.post('/msg/add', function(req, res)
  {
    let errId = req.body.errId1;
    let lang = req.body.lang1;
    let msg = req.body.msg1;

    session
      .run('create (n:Message {name: $langParam, errId: $errParam, lable: $msgParam})', {errParam: errId, langParam: lang, msgParam: msg})
      .then(function(result)
        {
          session
            .run('match (a:Error {name: $errParam}), (b:Message {name: $langParam, errId: $errParam, lable: $msgParam}) merge (a)-[r:Has]->(b)', {errParam: errId, langParam: lang, msgParam: msg})
            .then(function(result1)
              {
                res.redirect('/');
                //session.close();
              })
            .catch(function(err)
              {
                console.log(err);
              });
        })
      .catch(function(err){
        console.log(err);
      });
    
    res.redirect('/');
  });

  app.post('/err/del', function(req, res)
  {
    let errCode = req.body.errId3;

    //console.log(errCode);

    session
      .run('match (a:Error {name: $errParam}) delete a', {errParam: errCode})
      .then(function(result)
        {
          res.redirect('/');
          //session.close();
        })
      .catch(function(err){
        console.log(err)
      });
    
    res.redirect('/');
  });

app.post('/msg/del', function(req, res)
  {
    let errId = req.body.errId2;
    let lang = req.body.lang2;
    let msg = req.body.msg2;

    //console.log(errId,lang,msg);
    session
      .run('match (a:Error {name: $errParam})-[r:Has]->(b:Message {name: $langParam, errId: $errParam, lable: $msgParam}) delete r', {errParam: errId, langParam: lang, msgParam: msg})
      .then(function(result)
        {
          session
            .run('match (a:Message {name: $langParam, errId: $errParam, lable: $msgParam}) delete a', {errParam: errId, langParam: lang, msgParam: msg})
            .then(function(result1)
              {
                res.redirect('/');
                //session.close();
              })
            .catch(function(err)
              {
                console.log(err);
              });
        })
      .catch(function(err){
        console.log(err);
      });
    res.redirect('/');
  }); 

app.listen(3000);
console.log('Server is upp');

module.exports = app;